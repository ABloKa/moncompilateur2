
//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {UNSIGNED_INT, BOOLEAN, FLOATING};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	enum TYPE type;
	if(!isDeclared(lexer->YYText())){
		Error("Variable is not declared !");
		exit(-1);
	} 
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	if (current==NUMBER){
		return UNSIGNED_INT;
	}
	else{
		Error("Unsigned int expected !");
		exit(-1);
	}
}

void Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		return Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			return Number();
	     	else
				if(current==ID)
					return Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	OPMUL mulop;
	TYPE a = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		TYPE b = Factor();
		if (a == b){
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(mulop){
				case AND:
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND"<<endl;	// store result
					break;
				case MUL:
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
					break;
				case DIV:
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
					break;
				case MOD:
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD"<<endl;		// store result
					break;
				default:
					Error("opérateur multiplicatif attendu");
			}
		}
		else {
			Error("syntax error, doing a multiplive operation between two differents types !");
			exit(-1)
		}
	}
	return a;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE a = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		TYPE b = Term();
		if(a==b){
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(adop){
				case OR:
					cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
					break;			
				case ADD:
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					break;			
				case SUB:	
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
					break;
				default:
					Error("opérateur additif inconnu");
			}
			cout << "\tpush %rax"<<endl;// store result
		}
		else {
			Error("syntax error, doing an arithmetic expression between two differents types !");
			exit(-1)
		}		
	}
	return a;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE a = SimpleExpression();
		if(current==RELOP){
			oprel=RelationalOperator();
			TYPE B = SimpleExpression();
			if(a==b){
				cout << "\tpop %rax"<<endl;
				cout << "\tpop %rbx"<<endl;
				cout << "\tcmpq %rax, %rbx"<<endl;
				switch(oprel){
					case EQU:
						cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
						break;
					case DIFF:
						cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
						break;
					case SUPE:
						cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
						break;
					case INFE:
						cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
						break;
					case INF:
						cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
						break;
					case SUP:
						cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
						break;
					default:
						Error("Opérateur de comparaison inconnu");
				}
				cout << "\tpush $0\t\t# False"<<endl;
				cout << "\tjmp Suite"<<TagNumber<<endl;
				cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
				cout << "Suite"<<TagNumber<<":"<<endl;
				
				return BOOLEAN;
			}
			else{
				Error("syntax error, comparing two differents types !");
				
			}
					
		}
	else {
		return a;
	}
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	Expression();
	cout << "\tpop "<<variable<<endl;
}

void Statement();

void IfStatement();

void ForStatement();

void WhileStatement();

void BlockStatement();

// Statement := AssignementStatement
void Statement(void){
	if (current==ID){
		AssignementStatement();
	}
	if (strcmp(lexer->YYText(),"if")==0){
		IfStatement();
	}
	if (strcmp(lexer->YYText(),"while")==0){
		WhileStatement();
	}
	if (strcmp(lexer->YYText(),"for")==0){
		ForStatement();
	}
	if (strcmp(lexer->YYText(),"begin")==0){
		BlockStatement();
	}
}

void IfStatement(void){
	current=(TOKEN) lexer->yylex();
		Expression();
		cout<<"#if :"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq $0, %rax"<<endl;
		cout<<"\tje Else"<<endl;
		if (strcmp(lexer->YYText(),"then")==0){
			current=(TOKEN) lexer->yylex();
			cout<<"#then :"<<endl;
			Statement();
			cout<<"\tjmp Endif"<<endl;
			
		}
		else {
			Error("'then' expected !");
		}
		cout<<"Else:"<<endl;
		if ((strcmp(lexer->YYText(),"else")==0)){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		cout<<"Endif:"<<endl;
}

void WhileStatement(void){
	cout<<"Redo:"<<endl;
	current=(TOKEN) lexer->yylex();
		Expression();
		cout<<"#while : "<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq $0, %rax"<<endl;
		cout<<"\tje Endwhile"<<endl;
		if (strcmp(lexer->YYText(),"do")==0){
			cout<<"#do : "<<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			cout<<"\tjmp Redo"<<endl;
		}
		else {
			Error("'do' expected !");
		}
		cout<<"Endwhile:"<<endl;
}

void ForStatement(void){
	cout<<"#for : "<<endl;
	current=(TOKEN) lexer->yylex();
	string var = lexer->YYText();
		AssignementStatement();
		if ((strcmp(lexer->YYText(),"to")==0)){
			cout<<"#to : "<<endl;
			cout<<"To:"<<endl;
			current=(TOKEN) lexer->yylex();
			Expression();
			cout<<"\tpop %rax"<<endl;
			cout<<"\tcmpq "<<var<<",%rax"<<endl;
			cout<<"\tje Endfor"<<endl;
			if ((strcmp(lexer->YYText(),"do")==0)){
				cout<<"#do : "<<endl;
				current=(TOKEN) lexer->yylex();
				Statement();
				cout<<"\taddq $1,"<<var<<endl;
				cout<<"\tjmp To"<<endl;
			}
			else {
				Error("'do' expected !");
			}
		}
		else {
			Error("'to' expected !");
		}
		cout<<"Endfor:"<<endl;
	}
	
void BlockStatement(void){
	current=(TOKEN) lexer->yylex();
		cout<<"#begin : "<<endl;
		cout<<"Begin:"<<endl;
		Statement();
		while(current==SEMICOLON){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if ((strcmp(lexer->YYText(),"end")==0)){
			current=(TOKEN) lexer->yylex();
			cout<<"#end : "<<endl;
		}
	}
		
	
	

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





