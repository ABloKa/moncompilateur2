# CERIcompiler 

**Important :**

**Ce compilateur n'a qu'une seule version !**

*Description du langage :* 

Aucun type n'est encore déclaré pour le moment ! (**a+3 est déclaré correct par le langage, même si a n'est pas déclaré**)

Ce langage n'a pas de 'type' fonction ! (**vous écrivez vos codes à la suite, comme un main en pyhton !**)

*Fonctionnement du langage :*

Calculs mathématiques simples ( `+`, `-`, `*`, `/`, `%`); *(% = division euclidienne)*

Comparaisons entre deux objets (<, <=, ==, !=, >=, >)

Variables simples (*déclaration de variables 'x:=3' ou y:=7+x représentés par des lettre*s)

*Les conditions :*


> IF :  

le if est lu comme ceci :

if 'comparaison' //si la comparaison est vrai 

then

'calcul mathematique' ou 'declaration variable' 

**Attention : Si vous n'ajoutez pas de begin, seule la première ligne sera prise en compte ! Si vous mettez plusieurs calculs ou déclarations à la suite, le langage sortira du if !** 

else //sinon 
(**le else n'est pas obligatoire si vous ne voulez pas de condition contraire à votre if, si la condition n'est pas remplie, vous voulez que le programme ne fasse rien !**)

'calcul mathematique' ou 'declaration variable'

**Attention : Si vous n'ajoutez pas de begin, seule la première ligne sera prise en compte ! Si vous mettez plusieurs calculs ou déclarations à la suite, le langage sortira du else !**


> WHILE :  

while 'comparaison' //tant que la comparaison est vrai

do //faire

'calcul mathematique' ou 'declaration variable' 

**Attention : Si vous n'ajoutez pas de begin, seule la première ligne sera prise en compte ! Si vous mettez plusieurs calculs ou déclarations à la suite, le langage déclarera une erreur du type :'done expected !'**

done //fin du while


> FOR : 

for 'variable déclarée' //pour une variable à une valeur déclarée

to 'comparaison' // tant que la variable déclarée n'est pas atteinte (**Attention, seule une incrémentation de 1 est possible dans ce langage !**)

do  //va faire la ligne suivante (ou les lignes suivantes si cas de begin)

'calcul mathematique' ou 'declaration variable' //

**Attention : Si vous n'ajoutez pas end begin, seule la première ligne sera prise en compte !**


> BEGIN :  

**Le Begin va permettre de pouvoir mettre plusieurs calculs ou déclarations de variables dans vos if, for et while !**

**On vous conseille donc de le mettre par défaut pour vous habituer à faire de longs codes !**

begin //début du block de code

'calcul mathematique' ou 'declaration variable' `;` 

'calcul mathematique' ou 'declaration variable'

end //fin du block de code

**note : vous pouvez ajouter autant de déclarations de variables ou calculs mathématiques (vous pouvez mettre les deux dans le même block) mais vous devez obligatoirement séparer chaque calcul ou déclaration par un `;` !**

**note 2 : pas de `;` avant le `end` !**

*Exemple de code :* 

`
a = 6

b = a + 3

if b>a

then

a = a + b

else

begin

b = b + 1;

a = a - 2

end 

c = a +  1

while a<c

do 

a = a + 1

done

for d=0

to 9

do

begin 

a = a + 1;

b = b + 1;

c = a + b + 1

end

`

notes : il ne faut pas sauter 1 ligne dans une condition (laisser une ligne en blanc, je n'ai pas eu le choix à cause du type de ficher !')